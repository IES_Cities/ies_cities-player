IES Cities Player
=================

The IES Cities Player is intended to become a showroom of the applications developed within the IES Cities framework.

The code has been updated to support the structure of new cordova/phonegap projects, where the idea is to code once, and release to any desired platform. As the scope of IES Cities is actually just **android**, once downloaded the following command must be executed:
    
    cordova platform add android
   
This should create a platforms/android folder. Running 
   
    cordova build android
    
All the necessary code to run an Android application is compiled. This folder together with **CordovaLib** can be imported in your favourite IDE as an Android Project and work from there.
    
The application can also be directly installed in a connected device using:

    cordova run android

It is mandatory to have cordova 3.6 installed in the system, following the instructions provided [here](http://cordova.apache.org/docs/en/3.6.0/guide_cli_index.md.html#The%20Command-Line%20Interface).