
var SERVER_BASE_URL = "https://iescities.com/"
var SERVER_API_BASE_URL = SERVER_BASE_URL + "IESCities/api/"

var APIManager = {
    getServerBaseURL: function() {
        return SERVER_BASE_URL;
    },

    getApplicationByID: function(app_id) {
        return SERVER_API_BASE_URL + "entities/apps/" + app_id;
    },

    getAllApplications: function() {
        return SERVER_API_BASE_URL + "entities/apps/all";
    },

    getApplicationsMatchingKeywords: function(search_text) {
        return SERVER_API_BASE_URL + "entities/apps/search/?keywords=" + search_text;
    },

    getApplicationsNearGeopoint: function(latitude, longitude, radius) {
        return SERVER_API_BASE_URL + "entities/apps/near/" + latitude + "," + longitude + "/" + radius;
    },

    getDatasetsForApplicationByID: function(app_id) {
        return SERVER_API_BASE_URL + "entities/apps/" + app_id + "/datasets";
    },

    getRatingsForApplicationByID: function(app_id) {
        return SERVER_API_BASE_URL + "entities/apps/" + app_id + "/ratings";
    },
};
