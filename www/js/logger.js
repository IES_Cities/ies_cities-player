
 /*
 /  Configuration variables for the logging library.
 /
 /  app_name MUST be different for each app within IES Cities
*/

var jq = jQuery;

// Set to true to use the new POST mechanism on the event logging
var usePOST = true;

var useDeviceID = true;

var app_name = "IES Cities Player";

var MAX_SESSION_ID = 100000000;

 /*
 / ----------------------------------------------------------------------------------------------------
*/

// Create an object to perform logging actions in Web Applications
var Restlogging = {
    generateRandomID: function() {
        return (Math.floor(Math.random() * MAX_SESSION_ID));
    },

    init: function(server) {
        var session_id = 0;

        // Set up with a fresh (random) session ID
        if (useDeviceID) {
            //Generate and store a persistent anonymous device ID
            if (typeof (Storage) !== "undefined") {
                if (localStorage.session_id !== 'undefined') {
                    session_id = localStorage.session_id;
                    console.log("(Logger) Re-using device ID: " + session_id);
                }
                else {
                    // Create a random negative ID
                    session_id = -1 * Restlogging.generateRandomID();
                    localStorage.session_id = session_id;
                    console.log("(Logger) Creating a new device ID: " + session_id);
                }
            }
            else {
                // Device does not support local storage
                console.log("Sorry, your device does not support local storage for Device ID");
                // For now use a random session ID
                session_id = Restlogging.generateRandomID();
            }
        }
        else {
            // Use a random session ID
            session_id = Restlogging.generateRandomID();
        }

        localStorage.app_name = app_name;
        localStorage.server_url = server;
        localStorage.base_dir = 'IESCities/api/log/app';
        localStorage.session_id = session_id;

        console.log("(Logger) Initialised " + localStorage.app_name + " with session ID: " + localStorage.session_id);

        Restlogging.bindEvents();

        // Force logAppStart() as first launch of the application
        Restlogging.logAppStart();
    },

    // Bind common events to monitor the time an app is displayed on the screen
    // Losing focus on an app is treated as logAppStop() and resuming it as logAppStart()
    bindEvents: function() {
        document.addEventListener('backbutton', Restlogging.backButton, false);
        //document.addEventListener('menubutton', Restlogging.logAppStop, false);
        document.addEventListener('pause', Restlogging.logAppStop, false);
        document.addEventListener('resume', Restlogging.logAppStart, false);
    },
    
    backButton: function() {
    	if (window.location.pathname.indexOf('index.html') > -1) {
    		Restlogging.logAppStop();
    		navigator.app.exitApp();
    	}
    	else
    		window.history.back();
    },

    // POST request to logging API URL, sending data to the server
    postURL: function(url, post_data) {
        console.log("(Logger) URL for the POST request is: " + url);

        // Add instrumentation to time the call end to end
        var start_time = (new Date).getTime();

        jq.ajax ({
            dataType: "text",
            type: "POST",
            url: url,
            crossDomain: true,
            data: post_data,
            cache: false,
            async: false,

            success: function(data) {
                var elapsed_time = (new Date).getTime() - start_time;

                console.log("(Logger) Success log with messagge " + data + " in " + elapsed_time + "ms");
            },

            error: function(jqXHR, textStatus, errorThrown) {
                console.log("(Logger) Failure: " + errorThrown);
            }
        });
    },

    // GET request to logging API URL, checking the returned results against the expected outcome
    getURL: function(url, expected) {
        console.log("(Logger) URL for the GET request is: " + url);

        // Add instrumentation to time the call end to end
        var start_time = (new Date).getTime();

        jq.ajax ({
            dataType: "text",
            type: "GET",
            url: url,
            crossDomain: true,
            cache: false,
            async: false,

            success: function(data) {
                var elapsed_time = (new Date).getTime() - start_time;

                console.log("(Logger) Success log with messagge " + data + " in " + elapsed_time + "ms");
            },

            error: function(jqXHR, textStatus, errorThrown) {
                console.log("(Logger) Failure: " + errorThrown);
            }
        });
    },

    // Logging at app level
    logAppMessage: function(event_type, message) {
        console.log("(Logger) Logging an " + event_type + " event with message: " + message);

        // Gets the current time in seconds, with millisecond precision
        var timestamp = (new Date).getTime() / 1000;

        var log_api_url = "";

        log_api_url += localStorage.server_url;
        log_api_url += localStorage.base_dir;

        log_api_url += usePOST ? "/event" : "/stamp";
        log_api_url += "/" + timestamp;
        log_api_url += "/" + localStorage.app_name;
        log_api_url += "/" + localStorage.session_id;
        log_api_url += "/" + event_type;
        log_api_url += "/" + message;

        if (usePOST) {
            Restlogging.postURL(log_api_url, "logged");
        }
        else {
            Restlogging.getURL(log_api_url, "logged");
        }
    },

    // Logging at app level
    logPerformanceMessage: function(event_type, message, duration) {
        console.log("(Logger) Logging an " + event_type + " event with message: " + message);

        // Gets the current time in seconds, with millisecond precision
        var timestamp = (new Date).getTime() / 1000;

        var log_api_url = "";

        log_api_url += localStorage.server_url;
        log_api_url += localStorage.base_dir;

        log_api_url += "/perf";
        log_api_url += "/" + timestamp;
        log_api_url += "/" + localStorage.app_name;
        log_api_url += "/" + localStorage.session_id;
        log_api_url += "/" + event_type;
        log_api_url += "/" + duration;
        log_api_url += "/" + message;

        Restlogging.postURL(log_api_url, "logged");
    },

    logAppCustom: function(message){
        Restlogging.logAppMessage("AppCustom", message);
    },

    logAppStart: function() {
        localStorage.last_timestamp = (new Date).getTime();

        Restlogging.logAppMessage("AppStart", "Phone event");
    },

    // Not technically closing the app, but when the app loses its focus
    logAppStop: function() {
        var activeFor = (new Date).getTime() - localStorage.last_timestamp;

        console.log("Application active for: " + activeFor);
        Restlogging.logAppMessage("AppStop", "Phone event");

        // Also create app usage time log
        Restlogging.logPerformanceMessage("AppStop", "App active for", activeFor);
    },

    // TO-DO : Review features from this point to the end of the file, remove if irrelevant

    appTime: function(func, testname) {
        console.log(func);

        // Test the overhead of spawning a log
        var loops = 10
        var starttime = (new Date).getTime();

        for (var i = 0; i < loops; i++) {
            func.call(this); // This calls function in the correct context
        }

        var endtime = (new Date).getTime() - starttime;

        var div = document.body.children[1];
        var span = document.createElement('span')

        span.innerHTML = "<p> Time for test [" + testname + "] is " + endtime/loops + " ms";

        // Add the performance log to the database
        Restlogging.logPerformanceMessage("AppCustom", testname, Math.floor(endtime/loops));

        div.appendChild(span)
    },

    restGet: function(url, response) {
        Restlogging.getURL (localStorage.server_url + localStorage.base_dir + url, response);
    },

    countLogApp: function() {
        Restlogging.restGet("/getcountlogs", "");
    },

    // Test to see if the api is present where we think if is
    testAppAPI: function() {
        Restlogging.restGet("/here", "yes");
    },

    appTest: function() {
        Restlogging.appTime(Restlogging.logAppStart, "single app log");
        Restlogging.appTime(Restlogging.countLogApp, "get number of logged calls");
    }
};
