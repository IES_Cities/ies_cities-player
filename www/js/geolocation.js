
var GeoLocation = {
    currentPosition: function(callback) {
        if (navigator.geolocation) {
            console.log("Geolocation enabled");

            function showPosition(position) {
                var pos = {};

                pos.latitude = position.coords.latitude;
                pos.longitude = position.coords.longitude;

                console.log("lat/lng: [" + pos.latitude + ", " + pos.longitude + "]");

                callback(pos);
            }

            function errorHandler(err) {
                if (err.code == 1) {
                    console.log("Error: Access is denied!");
                }
                else if ( err.code == 2) {
                    console.log("Error: Position is unavailable!");
                }
            }

            var options = {};

            navigator.geolocation.getCurrentPosition(showPosition, errorHandler, options);
        }
        else {
            console.log("Geolocation is not supported by this browser.");

            return null;
        }
    },

    displayMap: function() {
        console.log("displaying map...");

        var actual_path = window.location.pathname;

        var location_href = "map.html";

        if (actual_path.indexOf("index.html") != -1) {
            location_href = "templates/" + location_href;
        }

        window.location = location_href;
    }
};
