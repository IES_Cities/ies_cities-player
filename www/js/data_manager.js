
var app_length = 0;
var app_counter = 0;

function saveAppToLocalStorage(app, callback) {
    localStorage.setItem("app_" + app.index, JSON.stringify(app));

    app_counter++;
};

function onAvailabilitySuccess(index, appArray, callback) {

    var appAvailable = function() {
        console.log(appArray[index].packageName + ' is available');
        appArray[index].available = true;
        //saveAppToLocalStorage(app, callback);
        if(appArray.length - 1 == index) {
            if (typeof callback == 'function') { 
                callback(appArray);
            }
        }
    }

    return appAvailable;
};

function onAvailabilityError(index, appArray, callback) {

   var appUnavailable = function() {
        console.log(appArray[index].packageName + ' is NOT available');
        appArray[index].available = false;
        //saveAppToLocalStorage(app, callback);
        if(appArray.length - 1 == index) {
            if (typeof callback == 'function') { 
                callback(appArray);
            }
        }
    }

    return appUnavailable;
};

var _dataManager = {
    getApplicationsFromAPI: function(url, callback) {
        console.log("calling getApplicationsFromAPI()...");

        $.getJSON(url)
            .done(function( json ) {
                console.log("API call to: " + url);

                var numberOfApps = 0;

                var app_list = [];

                if ($.isArray(json)) {
                    for (var i = 0; i < json.length; i++) {
                        if (json[i].packageName != 'eu.iescities.player') {
                            app_list.push(json[i]);
                        }
                    }
                }
                else {
                    if (json.packageName != 'eu.iescities.player') {
                        app_list.push(json);
                    }
                }

                console.log("Found " + app_list.length + " apps");

                app_length = app_list.length;
                app_counter = 0;

                for
                 (var i = 0; i < app_list.length; i++) {
                    var app = app_list[i];

                    console.log("Checking availability..." + app.packageName);

                    app.index = i;

                    appAvailability.check(
                        app.packageName,
                        onAvailabilitySuccess(i, app_list, callback),
                        onAvailabilityError(i, app_list, callback)
                    );
                    //numberOfApps++;
                }

                //localStorage.numberOfApps = numberOfApps;

                console.log("Search finished");

                if(app_list.length == 0) {
                    if (typeof callback == 'function') { 
                        callback(app_list);
                    }
                }
            })

            .fail(function( jqxhr, textStatus, error ) {
                var err = textStatus + ", " + error;
                console.log("Request Failed: " + err);
            });
    },

    saveAppsToLocalStorage: function(apps, callback) {
        for (var i = apps.length - 1; i >= 0; i--) {
            saveAppToLocalStorage(apps[i]);
        };
        localStorage.numberOfApps = apps.length;
        
        if (typeof callback == 'function') { 
            callback();
        }
    },

    getDatasetsFromAPI: function(url, callback) {
        console.log("calling getDatasetsFromAPI()...");
        console.log(url);

        $.getJSON(url)
            .done(function( json ) {
                console.log("API call to: " + url);

                var numberOfDatasets = 0;

                var dataset_list = [];

                if ($.isArray(json)) {
                    for (var i = 0; i < json.length; i++) {
                        dataset_list.push(json[i]);
                    }
                }
                else {
                    dataset_list.push(json);
                }

                console.log("Found " + dataset_list.length + " datasets");

                for (var i = 0; i < dataset_list.length; i++) {
                    var dataset = dataset_list[i];

                    dataset.index = i;

                    localStorage.setItem("dataset_" + i, JSON.stringify(dataset));
                    numberOfDatasets++;
                }

                localStorage.numberOfDatasets = numberOfDatasets;

                console.log("Search finished");

                callback();
            })

            .fail(function( jqxhr, textStatus, error ) {
                var err = textStatus + ", " + error;
                console.log("Request Failed: " + err);
            });
    },

    getCommentsFromAPI: function(url, callback) {
        console.log("calling getDatasetsFromAPI()...");
        console.log(url);

        $.getJSON(url)
            .done(function( json ) {
                console.log("API call to: " + url);

                var numberOfComments = 0;

                var comment_list = [];

                if ($.isArray(json)) {
                    for (var i = 0; i < json.length; i++) {
                        comment_list.push(json[i]);
                    }
                }
                else {
                    comment_list.push(json);
                }

                console.log("Found " + comment_list.length + " comments");

                for (var i = 0; i < comment_list.length; i++) {
                    var comment = comment_list[i];

                    comment.index = i;

                    localStorage.setItem("comment_" + i, JSON.stringify(comment));
                    numberOfComments++;
                }

                localStorage.numberOfComments = numberOfComments;

                console.log("Search finished");

                callback();
            })

            .fail(function( jqxhr, textStatus, error ) {
                var err = textStatus + ", " + error;
                console.log("Request Failed: " + err);
            });
    },
};
