
var DEFAULT_RADIUS = 10;

function defaultFor(arg, val) {
    return typeof arg !== 'undefined' ? arg : val;
};

var IESCitiesPlayer = {
    checkNetworkState: function(callback) {
        console.log("Checking network state...");

        $.ajax({
            url: APIManager.getAllApplications(),

            success: function(result){
                console.log("Network state OK");
                callback();
            },

            error: function(result){
                alert('The server seems to be down');
            }
        });
    },

    clearLocalStorage: function() {
        console.log("clearing localStorage...");

        var session_id = localStorage.session_id;
        var server_url = localStorage.server_url;
        var base_dir = localStorage.base_dir;
        var app_name = localStorage.app_name;
        var last_timestamp = localStorage.last_timestamp;
        var tos_read = localStorage.tos_read;

        localStorage.clear();

        localStorage.session_id = session_id;
        localStorage.server_url = server_url;
        localStorage.base_dir = base_dir;
        localStorage.app_name = app_name;
        localStorage.last_timestamp = last_timestamp;
        localStorage.tos_read = tos_read;
        
        console.log("...localStorage cleared");
    },

    keywordSearch: function(search_text) {
        console.log("Searching by keywords...");

        localStorage.keywords = search_text;

        Restlogging.logAppMessage("PlayerAppSearch", search_text);

        var url = APIManager.getApplicationsMatchingKeywords(search_text);

        _dataManager.getApplicationsFromAPI(url, function (json) {
            _dataManager.saveAppsToLocalStorage(json, function() {
                console.log("Refreshing index template");

                window.location.reload();
            });
        });
    },

    getStarsRating: function(rating) {
        var full = Math.floor(rating);
        var decimal = rating - Math.floor(rating);
        var half = (decimal > 0.25 && decimal < 0.75) ? 1 : 0;

        var split_rating = {
            full: full,
            half: half,
            empty: 5 - full - half
        }

        return split_rating;    
    },

    showApps: function(callback) {
        console.log("calling showApps()...");

        console.log("Displaying apps");

        for ( var i = 0; i < localStorage.numberOfApps; i++) {
            var appString = localStorage.getItem("app_" + i);
            var app = JSON.parse(appString);

            var item_template = Handlebars.templates['app_item'];
            $('.carousel-inner').append(item_template(app));

            var indicator_template = Handlebars.templates['carousel_indicator'];
            $('.carousel-indicators').append(indicator_template({number: i}));

            var rating = IESCitiesPlayer.getStarsRating(app.googlePlayRating);
            var rating_template = Handlebars.templates['app_rating'];
            $("#rating-" + i).append(rating_template(rating));
        };

        callback();
    },

    getAppsByLocation: function(radius, unit) {
        localStorage.radius = defaultFor(radius, DEFAULT_RADIUS);
        console.log("radius stablished");

        for (var i = 0; i < localStorage.length; i++) {
            var key = localStorage.key(i);
            if (key.startsWith('mapApp_')) {
                localStorage.removeItem(key);
            }
        }

        GeoLocation.currentPosition(function(pos) {
            var latitude = pos.latitude;
            var longitude = pos.longitude;
            if(unit == 'km')
                var url = APIManager.getApplicationsNearGeopoint(latitude, longitude, localStorage.radius);
            else
                var url = APIManager.getApplicationsNearGeopoint(latitude, longitude, localStorage.radius / 1000.0);
            Restlogging.logAppMessage("PlayerAppSearch", localStorage.radius + " " + unit + " radius from [" + latitude + ", " + longitude + "]");

            _dataManager.getApplicationsFromAPI(url, function (apps) {
                for (var i = 0; i < apps.length; i++) {
                    localStorage.setItem("mapApp_" + apps[i].appId, JSON.stringify(apps[i]));
                };

                console.log("moving to new template");

                GeoLocation.displayMap();
            });
        });
    },

    getAllApps: function(callback) {
        GeoLocation.currentPosition(function(pos) {
            var url = APIManager.getAllApplications();

            _dataManager.getApplicationsFromAPI(url, function (json) {
                if(typeof callback == 'function') {
                    callback(json);
                }
            });
        });
    },

    getAppInfo: function(app_id) {
        localStorage.setItem('mapSelectedApp', app_id);
        window.location = "view_app.html";
    },

    displayAppInfo: function() {
        var key = 'mapApp_' + localStorage.getItem('mapSelectedApp');
        var appString = localStorage.getItem(key);
        var app = JSON.parse(appString);
        app.index = app.appId;

        Restlogging.logAppCustom("Viewing app details: " + app.packageName);

        var item_template = Handlebars.templates['app_item'];
        $('#app').append(item_template(app));

        var rating = IESCitiesPlayer.getStarsRating(app.googlePlayRating);
        var rating_template = Handlebars.templates['app_rating'];
        $('#rating-' + app.index).append(rating_template(rating));
    },

    displayDatasetInfo: function() {
        for ( var i = 0; i < localStorage.numberOfDatasets; i++) {
            var datasetString = localStorage.getItem("dataset_" + i);
            var dataset = JSON.parse(datasetString);

            var template = Handlebars.templates['dataset_item'];
            $('#datasets').append(template(dataset));
        }
    },

    displayCommentInfo: function(callback) {
        for ( var i = 0; i < localStorage.numberOfComments; i++) {
            var commentString = localStorage.getItem("comment_" + i);
            var comment = JSON.parse(commentString);

            var template = Handlebars.templates['comment_item'];
            $('#comments').append(template(comment));
        };

        callback();
    },

    displayAppMap: function(app_id, index) {
        var keywords = localStorage.keywords;
        var app = localStorage.getItem("app_" + index);
        if(app === "undefined") {
            var url = APIManager.getApplicationByID(app_id);
            app = _dataManager.getApplicationsFromAPI(url, function() {
                if(keywords != "undefined") {
                    localStorage.keywords = keywords;
                }
                IESCitiesPlayer.clearLocalStorage();
            });
        }
        localStorage.setItem("selectedAPP", app);

        GeoLocation.currentPosition(function(pos) {
            var url = APIManager.getApplicationByID(app_id);

            _dataManager.getApplicationsFromAPI(url, function (json) {
                localStorage.setItem("mapApp_0", JSON.stringify(json[0]));
                GeoLocation.displayMap();
            });
        });
    },

    displayAppDatasets: function(app_id, index) {
        var keywords = localStorage.keywords;
        var app = localStorage.getItem("app_" + index);
        if(app === "undefined") {
            var url = APIManager.getApplicationByID(app_id);
            app = _dataManager.getApplicationsFromAPI(url, function() {
                if(keywords != "undefined") {
                    localStorage.keywords = keywords;
                }
                IESCitiesPlayer.clearLocalStorage();
            });
        }
        localStorage.setItem("selectedAPP", app);

        var url = APIManager.getDatasetsForApplicationByID(app_id);

        Restlogging.logAppCustom("Viewing datasets for app ID: " + app_id);

        _dataManager.getDatasetsFromAPI(url, function() {

            var actual_path = window.location.pathname;
            var location_href = "app_datasets.html";

            if (actual_path.indexOf("index.html") != -1) {
                location_href = "templates/" + location_href;
            }

            window.location = location_href;
        });
    },

    displayAppComments: function(app_id, index) {
        var keywords = localStorage.keywords;
        var app = localStorage.getItem("app_" + index);
        if(app === "undefined") {
            var url = APIManager.getApplicationByID(app_id);
            app = _dataManager.getApplicationsFromAPI(url, function() {
                if(keywords != "undefined") {
                    localStorage.keywords = keywords;
                }
                IESCitiesPlayer.clearLocalStorage();
            });
        }
        localStorage.setItem("selectedAPP", app);
        var url = APIManager.getRatingsForApplicationByID(app_id);

        Restlogging.logAppCustom("Viewing comments for app ID: " + app_id);

        _dataManager.getCommentsFromAPI(url, function() {

            var actual_path = window.location.pathname;
            var location_href = "app_comments.html";

            if (actual_path.indexOf("index.html") != -1) {
                location_href = "templates/" + location_href;
            }

            window.location = location_href;
        });
    },

    showPromotedApps: function(callback) {
        console.log("calling showPromotedApps()...");

        var url = APIManager.getAllApplications();

        _dataManager.getApplicationsFromAPI(url, function (json) {
            _dataManager.saveAppsToLocalStorage(json, function() {
                console.log("Displaying apps");

                for (var i = 0; i < localStorage.numberOfApps; i++) {
                    var appString = localStorage.getItem("app_" + i);
                    var app = JSON.parse(appString);

                    var item_template = Handlebars.templates['app_item'];
                    $('.carousel-inner').append(item_template(app));

                    var indicator_template = Handlebars.templates['carousel_indicator'];
                    $('.carousel-indicators').append(indicator_template({number: i}));

                    var rating = IESCitiesPlayer.getStarsRating(app.googlePlayRating);
                    var rating_template = Handlebars.templates['app_rating'];
                    $("#rating-" + i).append(rating_template(rating));
                }

                if(typeof callback == 'function') {
                    callback();
                }
            });
        });
    },

    launchApplication: function(package_name) {
        Restlogging.logAppCustom("Launch intent: " + package_name);

        window.plugins.webintent.startApp({
                pkg: package_name,
            },
            function() {
                console.log('Launching app');
                Restlogging.logAppCustom("Launch correct: " + package_name);
            },
            function() {
                console.log('Failed to start application');
                Restlogging.logAppCustom("Launch failure: " + package_name);
            }
        );
    },
};
