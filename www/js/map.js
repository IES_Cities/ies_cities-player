var userMarker = L.AwesomeMarkers.icon({
    icon: 'certificate',
    prefix: 'fa',
    markerColor: 'green',
    iconColor: 'white',
    spin: true
});

var appMarker = L.AwesomeMarkers.icon({
    icon: 'star',
    prefix: 'fa',
    markerColor: 'blue',
    iconColor: 'white',
    spin: false
});

var map = L.map('map');

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {maxZoom: 18}).addTo(map);

map.locate({setView: true, maxZoom: 6});

function onLocationFound(e) {
    var radius = Math.round(e.accuracy / 2);
    var message = "You are within " + radius + " meters from this point";

    if (localStorage.radius && localStorage.unit) {
        if(localStorage.unit == 'km')
            var myRadius = localStorage.radius * 1000;
        else 
            var myRadius = localStorage.radius;
        var currentDiameter = L.circle(e.latlng, myRadius, {
            color: 'black ',
            fillColor: '#gray',
            fillOpacity: 0.25
        }).addTo(map);
        map.fitBounds(currentDiameter.getBounds());
        message = "You are searching within " + localStorage.radius + localStorage.unit + " from this point";
    }

    L.marker(e.latlng, {icon: userMarker}).addTo(map)
        .bindPopup(message, {
            maxWidth: $("#map").width() - 62
        }).openPopup();

    L.circle(e.latlng, radius, {
        color: '#111774 ',
        fillColor: '#9fa2c7',
        fillOpacity: 0.5
    }).addTo(map);
};

function onLocationError(e) {
    alert(e.message);
};

map.on('locationfound', onLocationFound);
map.on('locationerror', onLocationError);

for (var i = 0; i < localStorage.length; i++) {
    var key = localStorage.key(i);

    if (key.startsWith('mapApp_')) {
        var appString = localStorage.getItem(key);
        var app = JSON.parse(appString);
        var north_east_latitude = app.geographicalScope.northEastLatitude;
        var north_east_longitude = app.geographicalScope.northEastLongitude;

        var south_west_latitude = app.geographicalScope.southWestLatitude;
        var south_west_longitude = app.geographicalScope.southWestLongitude;

        var central_point_latitude = (parseFloat(south_west_latitude) + parseFloat(north_east_latitude)) / 2;
        var central_point_longitude = (parseFloat(south_west_longitude) + parseFloat(north_east_longitude)) / 2;

        var random = Math.random() * (0.005 - 0.001) + 0.001;

        var position = [central_point_latitude + random, central_point_longitude - random];
        var bounds = [
            [north_east_latitude, north_east_longitude],
            [south_west_latitude, south_west_longitude]
        ];

        var popup_template = Handlebars.templates['app_popup'];

        L.Icon.Big = L.Icon.Default.extend({
            options: {
                iconSize: new L.Point(35, 55),
        }});

        var bigIcon = new L.Icon.Big();
        var marker = L.marker(position, {icon: bigIcon}).bindPopup(popup_template(app), {
            maxWidth: $("#map").width() - 62
        }).addTo(map);
        L.rectangle(bounds, {color: "#38aadd", weight: 5}).addTo(map);
    }
}
